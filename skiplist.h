//===----------------------------------------------------------------------===//
//
//                         Peloton
//
// skiplist.h
//
// Identification: src/include/index/skiplist.h
//
// Copyright (c) 2015-17, Carnegie Mellon University Database Group
//
//===----------------------------------------------------------------------===//

#pragma once
#include <stdint.h>
#include <stddef.h>
#define MAX_LEVEL 100
namespace peloton {
namespace index {

/*
 * SKIPLIST_TEMPLATE_ARGUMENTS - Save some key strokes
 */
#define SKIPLIST_TEMPLATE_ARGUMENTS                                       \
  template <typename KeyType, typename ValueType, typename KeyComparator, \
            typename KeyEqualityChecker, typename ValueEqualityChecker>
    
template <typename  RawKeyType,
           typename ValueType,
           typename KeyComparator = std::less<RawKeyType>,
           typename KeyEqualityChecker = std::equal_to<RawKeyType>,
           typename ValueEqualityChecker = std::equal_to<ValueType>>

// Different kinds of nodes in skiplist
enum class NodeType {
    TowerNode,
    BottomNode,
    HeaderNode,
};

// Key Value could be Positive or Negative infinite
enum class KeyTypes {
    ActualKey,
    PosInfinite,
    NegInfinite,
};


// Wrapper for RawKeyType Used. Helps in handling infinite
class KeyType {
   public:
    // If type is +/-Inf then we use this key to compare
    RawKeyType key;
    // Denote whether the key value is +/-Inf or not
    KeyTypes type;
    // is_neg_inf() - Whether the key value is -Inf
    bool is_neg_inf() {
      return type == KeyTypes::NegInfinite;
    }
    //is_pos_inf() - Whether the key value is +Inf
    bool IsPosInf() {
      return type == KeyTypes::PosInfinite;
    }
};

// Defining Basic Node which all nodes will inherit
class BasicNode {
private:
    NodeType type;

protected:
    uint16_t height;
    KeyType key;
    BasicNode *right;

public:
  // is_last_node() : Check if this is the last node in a particular level
  inline bool is_last_node() {
    return ((right == NULL) ? true : false);
   }
  // set_height() : Sets the height of the node and checks for errors
  inline bool set_height(uint16_t h) {
    if (height > 0 && height < MAX_LEVEL) {
      height = h;
      return true; // return true if height is positive and below MAX LEVEL
    } 
    return false //else return false
   }

  inline uint16_t get_height() {
    return height;
   }

   // Following 4 functions help user to determine node type
  inline bool is_tower_node() {
    return ((type == NodeType::TowerNode) ? true : false);
   }

  inline bool is_bottom_node() {
    return ((type == NodeType::BottomNode) ? true : false);
   }

  inline bool is_header_node() {
    return ((type == NodeType::HeaderNode) ? true : false);
   }

  virtual NodeType get_type() {
    return type;
  }
};

// TowerNode will only have a down pointer which is extra
class TowerNode : public BasicNode {
private:
    BasicNode *down;
};

//Bottom Nodes will have the value and a boolean del_flag for GC
class BottomNode : public BasicNode {
private:
    ValueType value;
    bool del_flag;
};

// Header Nodes will have a down pointer
class HeaderNode : public BasicNode {
private:
  BasicNode *down;
};


class SkipList {

  HeaderNode *header_p;
  KeyComparator key_comp_obj;
  KeyEqualityChecker key_eq_obj;
  ValueEqualityChecker value_eq_obj;
  KeyType key_type;
  /*
  key_cmp_less() : Compare two keys for '<' relation
  If key1 < key2 return true
  If key1 >= key2 return false
  NOTE : Pos Inf and Neg Inf keys are not handled
  */

  bool key_cmp_less(KeyType &key1, KeyType &key2) const {

    // Case 1: Both Keys are Negative Infinite
    // Assert False
    if ((key1.is_neg_inf()) && (key2.is_neg_inf())) {
      fprintf (stderr, "ERROR: Both Keys are Negative Infinite\n");
      assert(false);
    }

    // Case 2: Key 1 is Negative Infinite and Key 2 is not Negative Infinite
    if ((key1.is_neg_inf()) && (!key2.is_neg_inf())) {
      return true;
    }

    //Case 3: Key 1 is not Negative Infinite and Key 2 is Negative Infinite
    if ((!key1.is_neg_inf()) && (key2.is_neg_inf())) {
      return false;
    }

    // Case 4: Both Keys are Positive Infinite
    // Assert False
    if ((key1.is_pos_inf()) && (key2.is_pos_inf())) {
      fprintf (stderr, "ERROR: Both Keys are Positive Infinite\n");
      assert(false);
    }

    // Case 5: Key 1 is Positive Infinite and Key 2 is not Positive Infinite
    if ((key1.is_pos_inf()) && (!key2.is_pos_inf())) {
      return false;
    }

    //Case 6: Key 1 is not Positive Infinite and Key 2 is Positive Infinite
    if ((!key1.is_pos_inf()) && (key2.is_pos_inf())) {
      return true;
    }

    return key_comp_obj(key1.key, key2.key);
  }

  /*
  key_cmp_greater_equal() : Compare two keys for '>=' relation
  NOTE : Pos Inf and Neg Inf keys are not handled
  Negate output for key_cmp_less()
  */

  inline bool key_cmp_greater_equal(KeyType &key1, KeyType &key2) const {
    return !key_cmp_less(key1, key2);
  }

  /*
  key_cmp_greater() : Compare two keys for '>' relation
  NOTE : Pos Inf and Neg Inf keys are not handled
  Flip Output for key_cmp_less()
  */
  inline bool key_cmp_greater(KeyType &key1, KeyType &key2) const {
    return key_cmp_less(key2, key1);
  }

  /*
  key_cmp_greater() : Compare two keys for '<=' relation
  NOTE : Pos Inf and Neg Inf keys are not handled
  Negate Output for key_cmp_greater()
  */
  inline bool key_cmp_less_equal(KeyType &key1, KeyType &key2) const {
    return !key_cmp_greater(key1, key2);
  }

  /*
  key_cmp_equal() : Compare two keys for equality relation
  NOTE : Pos Inf and Neg Inf keys are not handled
  */

  inline bool key_cmp_equal(KeyType &key1, KeyType &key2) const {
    if ((key1.is_neg_inf()) || (key1.is_pos_inf()) || (key2.is_neg_inf()) || (key2.is_pos_inf())) {
      fprintf(stderr, "ERROR: Comparing Infinite\n", );
      assert(false);
    }
    return key_eq_obj(key1.key, key2.key);
  }

  /*
  key_cmp_not_equal() : Compare two keys for inequality relation
  NOTE : Pos Inf and Neg Inf keys are not handled
  Negate Output for key_cmp_equal()
  */

  bool key_cmp_not_equal(KeyType &key1, KeyType &key2) const {
    return !key_cmp_equal(key1, key2);
  }



//Constructor

SkipList(KeyComparator p_key_cmp_obj = std::less<RawKeyType>{},
        KeyEqualityChecker p_key_eq_obj = std::equal_to<RawKeyType>{},
        ValueEqualityChecker p_value_eq_obj = std::equal_to<ValueType>{}) :
        key_comp_obj{p_key_cmp_obj},
        key_eq_obj{p_key_eq_obj},
        value_eq_obj{p_value_eq_obj} {
   return;
}

//Destructor

~SkipList() {
  return;
}

// get_wrapped_key() - Return an internally wrapped key type used to traverse the index
   
inline KeyType get_wrapped_key(RawKeyType key) {
  return KeyType {key, KeyTypes::ActualKey};
}

// get_pos_infinite() - Get a positive infinite key
// Assumes there is a trivial constructor for RawKeyType

inline KeyType get_pos_infinite() {
  return KeyType {RawKeyType{}, KeyTypes::PosInfinite};
}

// get_neg_infinite() - Get a negative infinite key
// Assumes there is a trivial constructor for RawKeyType

inline KeyType get_neg_infinite() {
  return KeyType {RawKeyType{}, KeyTypes::NegInfinite};
}

};
}  // End index namespace
}  // End peloton namespace
